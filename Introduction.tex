\section{Introduction}\label{sec:introduction}

%
% introduce approximate computing
%
Approximate computing is an emerging design paradigm for digital systems.
As energy consumption of digital systems grows rapidly,
energy efficiency has become an essential concern in their design~\cite{han2013approximate}.
Fortunately, many applications are error-resilient,
such as image processing, data mining, and machine learning.
Thus, they allow to introduce some errors into the overall function. If the errors are introduced properly,
the application-level quality is still maintained, but the area, delay, and power consumption of the circuits could be dramatically reduced. Therefore, approximate computing has received considerable attention recently.

This work focuses on \textbf{approximate logic synthesis} (\textbf{ALS}) for multi-level circuits.
ALS automatically generates inexact circuits of better quality from the original accurate circuits
under the constraints on some error metrics. Typical quality measures include circuit area
and delay, while typical error metrics include error rate and error magnitude.
To derive an approximate design, many existing works iteratively apply
\textbf{local approximate changes} (\textbf{LACs}) to the circuit.
Some representative LACs include simply replacing a gate by a constant 0/1~\cite{shin2011new}
and substituting a signal by another one with similar functionality~\cite{venkataramani2013substitute}.

%
% introduce the procedure of existing ALS techniques
%
Many existing ALS methods are iterative. In each iteration, they determine which LAC should be applied greedily~\cite{shin2011new, venkataramani2013substitute,  wu2016efficient, chandrasekharan2016approximation, yao2017approximate}.
Specifically, a score is calculated for each valid LAC based on its local quality improvement (such as area improvement) and induced error.
Then, the one with the highest score is selected.
The iteration terminates when a given error limit is reached.

%
% figure:greedy
%
\begin{small}
    \begin{figure}
        \centering
        \includegraphics[width=0.48\textwidth]{fig/greedy.png}
        \caption{\small
            A greedy ALS method is stuck into a local optimum.
            A node represents a circuit and an edge represents a local approximate change (LAC).
            $A$ and $E$ denote the area and error rate of the corresponding circuit, respectively.
            $S$ is the score of an LAC.
        }\label{fig:greedy}
    \end{figure}
\end{small}

%
% analyze the drawback of the greedy approach
%
However, a greedy ALS method has its natural drawback of getting stuck into a local optimum.
Fig.~\ref{fig:greedy} shows an example for this.
Assume that the error rate threshold is 1\%. The target is to synthesize a circuit with the minimum area
and error rate no more than 1\%.
Node \textcircled{1} is the input accurate circuit,
which has an area of 305 and no error (i.e., $A=305, E=0$).
Each edge corresponds to an LAC and its score $S$ is put near the edge.
If the method always chooses an LAC with the highest score and modifies the circuit with the choice each time,
it will move along the red path and generate an inexact design with area of 282 and error rate of 0.98\%.
Unfortunately, the best order leading to the best solution should be  \textcircled{1}$\to$\textcircled{2}$\to$\textcircled{4}$\to$\textcircled{7}.
Thus, a greedy ALS method does not guarantee to produce an approximate circuit with the best quality.

%
% introduce our idea
%
To overcome this issue,
we propose to \ch{utilize} two advanced search algorithms to find a better ordering \ch{of applying} LACs.
The first search algorithm we apply is beam search.
It is an extension of the basic greedy method by keeping track of multiple top-scored LACs.
The second is a recently proposed search technique,
\textbf{Monte Carlo tree search} (\textbf{MCTS}).
It is a method for finding optimal decisions by taking random samples in the search space.
It has been successfully applied to many planning problems~\cite{balla2009uct, bjarnason2009lower, mansley2011sample}. 
Our experimental results showed that these algorithms are effective in finding a better ordering than the basic greedy search and thus,
improve the quality of the final approximate circuits.

% introduce structure
The rest of this paper is organized as follows.
Section~\ref{sec:relatework} discusses the related work.
Section~\ref{sec:preliminary} presents the preliminaries and the basic ideas.
Sections~\ref{sec:beam_search} and~\ref{sec:mcts} describe the proposed ALS methods with beam search and Monte Carlo tree search, respectively.
Section~\ref{sec:result} shows the experimental results. Section~\ref{sec:conclusion} concludes our work.
