#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import xlrd
import sys


def test():
    x = np.linspace(0, 2, 100)

    plt.plot(x, x, label='linear')
    plt.plot(x, x**2, label='quadratic')
    plt.plot(x, x**3, label='cubic')

    plt.xlabel('x label')
    plt.ylabel('y label')

    plt.title("Simple Plot")

    plt.legend()

    plt.show()


def ReadData():
    data = xlrd.open_workbook('up-to-date data.xlsx')
    return data


def PlotTwo(table):
    plt.subplot(121)
    PlotGreedy(table)
    plt.subplot(122)
    PlotBS(table)
    plt.show()


def PlotGreedy(table):
    for i in range(2, 10):
        line = table.row_values(i)
        benchmark, area = line[0], int(line[3])
        er = np.array([0.000, 0.005, 0.010, 0.030, 0.050])
        areaRed = np.array([int(line[3]), int(line[6]), int(line[12]), int(line[18]), int(line[24])])
        areaRed = (area - areaRed) / area
        plt.plot(er * 100, areaRed * 100, marker='o', label=benchmark)

    plt.legend()
    plt.title("Iterative greedy flow", fontsize=13)
    plt.xlabel("Error rate (%)", fontsize=11)
    plt.ylabel("Area reduction ratio (%)", fontsize=11)


def PlotBS(table):
    for i in range(2, 10):
        line = table.row_values(i)
        benchmark, area = line[0], int(line[3])
        er = np.array([0.000, 0.005, 0.010, 0.030, 0.050])
        areaRed = np.array([int(line[3]), int(line[7]), int(line[13]), int(line[19]), int(line[25])])
        areaRed = (area - areaRed) / area
        plt.plot(er * 100, areaRed * 100, marker='o', label=benchmark)

    plt.legend()
    plt.title("BS-ALS flow", fontsize=13)
    plt.xlabel("Error rate (%)", fontsize=11)
    plt.ylabel("Area reduction ratio (%)", fontsize=11)


def PlotMCTS(table):
    for i in range(2, 10):
        line = table.row_values(i)
        benchmark, area = line[0], int(line[3])
        er = np.array([0.000, 0.005, 0.010, 0.030, 0.050])
        areaRed = np.array([int(line[3]), int(line[8]), int(line[14]), int(line[20]), int(line[26])])
        areaRed = (area - areaRed) / area
        plt.plot(er * 100, areaRed * 100, marker='o', label=benchmark)

    plt.legend(fontsize=13)
    plt.xlabel("Error rate (%)", fontsize=14)
    plt.ylabel("Area reduction ratio (%)", fontsize=14)
    plt.xticks(fontsize=13)
    plt.yticks(fontsize=13)


def PlotTime(table, circuit, oriArea, greedyArea):
    for i in range(table.ncols):
        value = table.col_values(i)
        it = []
        area = []
        benchmark = value[0]
        assert circuit[i] == benchmark
        if benchmark != 'c1908':
            continue
        for j in range(1, len(value)):
            if value[j] == '':
                break
            it.append(j)
            area.append(1 - float(value[j]) / oriArea[i])
        it, area = np.array(it), np.array(area)
        plt.plot(it, area, label="MCTS-ALS flow")
        linex = np.array([0, max(it)])
        liney = 1 - np.array([greedyArea[i], greedyArea[i]]) / oriArea[i]
        plt.plot(linex, liney, label="iterative greedy flow")
    plt.legend()
    plt.xlabel("Iteration time")
    plt.ylabel("Area reduction ratio (%)")
    plt.show()


def ParseLogFile(oriArea, greedyArea, fileName):
    f = open(fileName, "r")
    line = f.readline()
    cnt = -1
    minArea = oriArea
    while line != "":
        if line.startswith("-----------------"):
            cnt += 1
            if cnt:
                print(cnt, minArea)
        elif line.startswith("area = "):
            pos = line.find(" delay")
            minArea = min(minArea, int(line[7:pos]))
        line = f.readline()


if __name__ == "__main__":
    benchmark = ['c432', 'c499', 'c880', 'c1908', 'c2670', 'c3540', 'c5315', 'c7552']
    oriArea = [309, 792, 629, 747, 1374, 1915, 2408, 3328]
    greedyArea = [253, 720, 587, 441, 927, 1755, 2301, 2804]
    data = ReadData()
    table1 = data.sheets()[0]
    table2 = data.sheets()[1]
    # PlotTwo(table1)
    # PlotGreedy(table1)
    # PlotBS(table1)
    # PlotMCTS(table1)
    # plt.show()
    # PlotTime(table2, benchmark, oriArea, greedyArea)

    i = 4
    print(benchmark[i])
    # ParseLogFile(oriArea[i], greedyArea[i], 'mcts-time-0.01/' + benchmark[i] + '.log')
    ParseLogFile(oriArea[i], greedyArea[i], 'mcts-time-extra/c2670_10_0_05.log')
